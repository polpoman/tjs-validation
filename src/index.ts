import express from 'express';
import { StatusCodes } from 'http-status-codes';
import { Validator } from 'jsonschema';
import * as squareSchema from './json-schemas/square.json';

// express middleware
const squareValidator = (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) => {
    const v = new Validator();
    const result = v.validate(req.body, squareSchema);
    if (result.valid) { // valid square
        next(); 
    } else { // invalid square
        res.status(StatusCodes.BAD_REQUEST).json({errors: result.errors})
    }
};

const port = 3000;
const app = express();

app.use(express.json());
app.get('', (req, res) => res.json({status: 'ok'}));

app.post('/square', squareValidator, (req, res) => {
    res.json({status: 'ok', errors: []});
});

if (process.env.NODE_ENV !== 'test') {
    app.listen(port, () => console.log(`Server listening at localhost:${port}`));
}

export default app;