export interface Square {
    /**
     * @minimum 0
     */
    width: number;
    /**
     * @minimum 0
     */
    height: number;
}