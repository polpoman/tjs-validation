import 'jest';
import * as square from "../json-schemas/square.json";
import http from 'http';
import app from "../index";
import { options } from './helpers/http';
import { Square } from '../interfaces/square';
import createBody from './helpers/body';
import { StatusCodes } from 'http-status-codes';

let server: http.Server;

describe('Test framework is working', () => {
    beforeAll(() => {
        server = app.listen(3000, () => {});
    })
    it('JEST framework is working as expected', () => expect(true).toBe(true));

    // Vscode should be able to show JSON Schema properties through intelliSense.
    it('Can read the generated JSON Schema', () => {
        expect(typeof square === 'object').toBe(true);
    });

    it('Express works', (done) => {
        let req = http.request(options, (res) => {
            expect(res.statusCode).toBe(200);
            done();
        })
        req.end();
    });
    
    it('Express can validate a valid square object with JSON Schema', (done) => {
        const req = http.request({
            ...options,
            method: 'POST',
            path: '/square'
        }, (res) => {
            let chunks = [];
            res.on("data", (chunk) => chunks.push(chunk));
            res.on("end", () => {
                const body: any = createBody(chunks);
                expect(res.statusCode).toBe(200);
                expect(body.errors.length).toBe(0);
                done();
            })
        });
        const square: Square = { width: 1, height: 2 };
        req.write(JSON.stringify(square));
        req.end();
    });

    it('Express detects invalid square object with JSON Schema', (done) => {
        const req = http.request({
            ...options,
            method: 'POST',
            path: '/square'
        }, (res) => {
            let chunks = [];
            res.on("data", (chunk) => chunks.push(chunk));
            res.on("end", () => {
                const body: any = createBody(chunks);
                expect(res.statusCode).toBe(StatusCodes.BAD_REQUEST);
                expect(body.errors.length).toBeGreaterThan(0);
                for (const error of body.errors) {
                    console.log({error});
                }
                done();
            })
        });
        /** invalid square object, width less than required minimum */
        const square: Square = { width: -1, height: 2 };
        req.write(JSON.stringify(square));
        req.end();
    });

    afterAll((done) => {
        server.close(() => {
            done();
        });
    })
});
