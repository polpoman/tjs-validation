import http from 'http';

export const options: http.RequestOptions = {
    hostname: "localhost",
    port: 3000,
    path: "",
    headers: {
      "Content-Type": "application/json"
    }
};