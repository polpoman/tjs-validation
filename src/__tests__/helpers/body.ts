export const createBody = (buffers: Buffer[]): any => buffers.length > 0 ? JSON.parse(Buffer.concat(buffers).toString()) : undefined;

export default createBody;