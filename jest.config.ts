import { InitialOptionsTsJest } from "ts-jest/dist/types";

const options: InitialOptionsTsJest = {
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest"
  },
  testMatch: [
    //"**/__tests__/**/*.+(ts|tsx|js)",
    // "**/?(*.)+(spec|test).+(ts|tsx|js)"
    "**/__tests__/**/*?(*.)+(spec|test).ts"
  ],
  moduleFileExtensions: ["ts", "tsx", "js"],
}

export default options;