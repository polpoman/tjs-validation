import fs from 'fs';
import path from "path";
import * as tjs from "typescript-json-schema";

const settings: tjs.PartialArgs = {
    required: true
};
const compilerOptions: tjs.CompilerOptions = {
    strictNullChecks: true
};

const basePath = ["src","interfaces"];
const program = (filename: string) => {
    console.log(path.resolve(...basePath,filename));
    return tjs.getProgramFromFiles(
        [path.resolve(...basePath,filename)],
        compilerOptions
    )
};
const saveDir = path.join(__dirname, '../', '../', 'src', 'json-schemas');

const schema = (filename: string, ifname: string) => {
    return tjs.generateSchema(
        program(filename), 
        ifname,
        settings
    );
}

export const JSONSchemaWriter = (filename: string, ifnames: string[]) => {
    if (!fs.existsSync(saveDir)) {
        fs.mkdirSync(saveDir, {recursive: true});
    }    
    for (const ifname of ifnames) {
        const generatedSchema = schema(filename, ifname);
        const JSONSchema = JSON.stringify(generatedSchema, null, 4);
        const savepath = path.join(
            saveDir,
            (filename.split('.')[0]+'.json')
        );
        fs.writeFileSync(savepath,JSONSchema,{ encoding: 'utf-8' });
    }
};

export default JSONSchemaWriter;