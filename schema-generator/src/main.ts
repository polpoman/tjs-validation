
import jsonSchemaWriter from "./schema-writer";

const main = () => {
    console.log('Writer started');
    jsonSchemaWriter('square.ts', ['Square']);
    console.log('JSON Schemas created');
}

export default main();